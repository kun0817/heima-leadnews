package com.heima.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.admin.dtos.SensitiveDto;
import com.heima.model.admin.pojo.AdSensitive;
import com.heima.model.common.dtos.ResponseResult;

public interface AdSensitiveService extends IService<AdSensitive> {

    /**
     * 分页查询敏感词列表
     * @param sensitiveDto
     * @return
     */
    ResponseResult pageList(SensitiveDto sensitiveDto);

    /**
     * 新增敏感词
     * @param adSensitive
     * @return
     */
    ResponseResult insert(AdSensitive adSensitive);

    /**
     * 修改敏感词
     * @param adSensitive
     * @return
     */
    ResponseResult updateSen(AdSensitive adSensitive);

    /**
     * 删除指定的敏感词
     * @param id
     * @return
     */
    ResponseResult del(Integer id);
}
