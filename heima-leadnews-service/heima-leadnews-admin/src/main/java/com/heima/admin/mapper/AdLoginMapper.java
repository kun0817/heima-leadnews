package com.heima.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.admin.pojo.AdUser;

public interface AdLoginMapper extends BaseMapper<AdUser> {
}
