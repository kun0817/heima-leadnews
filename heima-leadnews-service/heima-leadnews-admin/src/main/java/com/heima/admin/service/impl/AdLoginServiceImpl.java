package com.heima.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.admin.mapper.AdLoginMapper;
import com.heima.admin.service.AdLoginService;
import com.heima.model.admin.dtos.AdUserDto;
import com.heima.model.admin.pojo.AdUser;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.utils.common.AppJwtUtil;
import com.sun.org.apache.regexp.internal.RE;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
@Transactional
@Slf4j
public class AdLoginServiceImpl extends ServiceImpl<AdLoginMapper, AdUser> implements AdLoginService {

    /**
     * 用户登录
     * @param adUserDto
     * @return
     */
    @Override
    public ResponseResult login(AdUserDto adUserDto) {
        //1.校验参数
        if (adUserDto==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //2.判断用户名和密码是否为空
        if (StringUtils.isBlank(adUserDto.getName())||StringUtils.isBlank(adUserDto.getPassword())){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        //3.查询用户是否存在
        AdUser adUser = getOne(Wrappers.<AdUser>lambdaQuery().eq(AdUser::getName, adUserDto.getName()));
        if (adUser==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        //4.校验密码
        String password = adUserDto.getPassword();//用户输入的明文密码
        String salt = adUser.getSalt();//数据库中的盐
        String pwdEncrypt = DigestUtils.md5DigestAsHex((password + salt).getBytes());//md5加盐后加密的密码
        String pwdDB = adUser.getPassword();//数据库中原始密码
        if (pwdEncrypt.equals(pwdDB)){
            //根据用户id为登录成功的用户生成token字符串
            String token = AppJwtUtil.getToken(adUser.getId().longValue());
            Map userMap = new HashMap<>();
            userMap.put("token",token);
            adUser.setPassword("");
            adUser.setSalt("");
            userMap.put("user",adUser);
            return ResponseResult.okResult(userMap);//将map返回去
        }
        else {
            return ResponseResult.errorResult(AppHttpCodeEnum.LOGIN_PASSWORD_ERROR);
        }

    }

    /**
     * 用户注册
     * @param adUser
     * @return
     */
    @Override
    public ResponseResult register(AdUser adUser) {
        if (adUser==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        String salt = RandomStringUtils.randomAlphanumeric(10);//生成随机数配置盐
        String passWord = DigestUtils.md5DigestAsHex((salt + adUser.getPassword()).getBytes());
        log.info("盐是"+salt);
        adUser.setPassword(passWord);
        adUser.setSalt(salt);
        adUser.setStatus(1);
        adUser.setCreatedTime(new Date());
        adUser.setLoginTime(new Date());
        save(adUser);
        return ResponseResult.okResult(null);
    }
}
