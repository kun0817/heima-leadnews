package com.heima.admin.controller.v1;

import com.heima.admin.service.AdLoginService;
import com.heima.model.admin.dtos.AdUserDto;
import com.heima.model.admin.pojo.AdUser;
import com.heima.model.common.dtos.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/login")
@Api(value = "登录管理",tags = "login",description = "登录Api")
public class AdLoginController {
    @Autowired
    private AdLoginService adLoginService;

    @ApiOperation("用户登录")
    @PostMapping("/in")
    public ResponseResult login(@RequestBody AdUserDto adUserDto){
        return adLoginService.login(adUserDto);
    }

    @ApiOperation("用户注册")
    @PostMapping("/register")
    public ResponseResult register(@RequestBody AdUser adUser){
        return adLoginService.register(adUser);
    }
}
