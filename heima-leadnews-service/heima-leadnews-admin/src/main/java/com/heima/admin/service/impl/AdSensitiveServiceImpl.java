package com.heima.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.admin.mapper.AdSensitiveMapper;
import com.heima.admin.service.AdSensitiveService;
import com.heima.model.admin.dtos.SensitiveDto;
import com.heima.model.admin.pojo.AdSensitive;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
@Transactional
public class AdSensitiveServiceImpl extends ServiceImpl<AdSensitiveMapper, AdSensitive> implements AdSensitiveService {

    /**
     * 分页查询敏感词列表
     * @param sensitiveDto
     * @return
     */
    @Override
    public ResponseResult pageList(SensitiveDto sensitiveDto) {
        //1.校验参数
        if (sensitiveDto==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //2.获取分页信息
        sensitiveDto.checkParam();
        //3.封装分页条件并判断name
        //QueryWrapper<AdSensitive> queryWrapper = Wrappers.<AdSensitive>query().eq();
        LambdaQueryWrapper<AdSensitive> queryWrapper = new LambdaQueryWrapper<>();
        if (StringUtils.isNotBlank(sensitiveDto.getName())){
            queryWrapper.like(AdSensitive::getSensitives,sensitiveDto.getName());
        }
        Page<AdSensitive> page = new Page<>(sensitiveDto.getPage(), sensitiveDto.getSize());
        page(page,queryWrapper);
        new PageResponseResult(sensitiveDto.getPage(), sensitiveDto.getSize(), (int) page.getTotal());
        ResponseResult result = new PageResponseResult(sensitiveDto.getPage(), sensitiveDto.getSize(), (int) page.getTotal());
        result.setData(page.getRecords());
        return result;

    }

    /**
     * 新增敏感词
     * @param adSensitive
     * @return
     */
    @Override
    public ResponseResult insert(AdSensitive adSensitive) {
        //校验参数
        if (adSensitive==null||StringUtils.isBlank(adSensitive.getSensitives())){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //新增
        adSensitive.setCreatedTime(new Date());
        save(adSensitive);
        return ResponseResult.okResult(null);
    }

    /**
     * 修改敏感词
     * @param adSensitive
     * @return
     */
    @Override
    public ResponseResult updateSen(AdSensitive adSensitive) {
        //1.校验参数
        if (adSensitive==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //2.查询被修改
        AdSensitive sensitive = getById(adSensitive.getId());
        if (sensitive==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        //3.修改
        adSensitive.setCreatedTime(new Date());
        updateById(adSensitive);
        return ResponseResult.okResult(null);
    }

    /**
     * 删除指定敏感词
     */
    @Override
    public ResponseResult del(Integer id) {
        if (id==null||id==0){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //查询删除的敏感词
        AdSensitive sensitive = getById(id);
        if (sensitive==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        removeById(id);
        return ResponseResult.okResult(null);
    }


}
