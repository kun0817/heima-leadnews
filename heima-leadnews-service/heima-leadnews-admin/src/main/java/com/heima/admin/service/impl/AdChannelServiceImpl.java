package com.heima.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.model.admin.pojo.AdChannel;
import com.heima.model.admin.dtos.ChannelDto;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.admin.mapper.AdChannelMapper;
import com.heima.admin.service.AdChannelService;
import com.sun.org.apache.regexp.internal.RE;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;

@Service
@Transactional
public class AdChannelServiceImpl extends ServiceImpl<AdChannelMapper, AdChannel> implements AdChannelService {

    /**
     * 分页查询频道列表
     * @param channelDto
     * @return
     */
    @Override
    public ResponseResult list(ChannelDto channelDto) {
        //1.校验参数,分页参数不需要校验,有默认值
        if (channelDto==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //判断分页参数是否存在
        channelDto.checkParam();
        LambdaQueryWrapper<AdChannel> queryWrapper = new LambdaQueryWrapper<>();
        if (StringUtils.isNotBlank(channelDto.getName())){
            queryWrapper.like(AdChannel::getName,channelDto.getName());
        }
        Page<AdChannel> page = new Page<>(channelDto.getPage(), channelDto.getSize());
        page(page, queryWrapper);

        ResponseResult result = new PageResponseResult(channelDto.getPage(), channelDto.getSize(), (int) page.getTotal());
        result.setData(page.getRecords());
        return result;
    }

    /**
     * 增加频道
     * @param adChannel
     * @return
     */
    @Override
    public ResponseResult insert(AdChannel adChannel) {
        //校验参数
        if (adChannel==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        adChannel.setCreatedTime(new Date());
        adChannel.setIsDefault(true);
        save(adChannel);
        return ResponseResult.okResult(null);
    }

    /**
     * 修改频道信息
     * @param adChannel
     * @return
     */
    @Override
    public ResponseResult updata(AdChannel adChannel) {
        //校验参数
        if (adChannel==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //查询被修改的频道
        AdChannel channelById = getById(adChannel.getId());
        //判断是否存在
        if (channelById==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        adChannel.setCreatedTime(new Date());
        adChannel.setIsDefault(true);
        updateById(adChannel);
        //4.响应数据
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    /**
     * 根据id删除指定频道
     * @param id
     * @return
     */
    @Override
    public ResponseResult del(Integer id) {
        //1.校验参数
        if (id==null||id==0){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //2.根据id查询到指定频道
        AdChannel channel = getById(id);
        //3.判断是否存在
        if (channel==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        //4.存在的话判断是否启用,启用不能删除
        if(channel.getStatus()){
            return ResponseResult.errorResult(AppHttpCodeEnum.LOGIC_ERROR,"数据已启用,不得删除");
        }
        //没有启用则删除
        removeById(id);
        return ResponseResult.okResult(null);
    }
}
