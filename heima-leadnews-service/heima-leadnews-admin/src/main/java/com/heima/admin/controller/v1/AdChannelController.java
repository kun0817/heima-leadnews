package com.heima.admin.controller.v1;

import com.heima.model.admin.dtos.ChannelDto;
import com.heima.model.admin.pojo.AdChannel;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.admin.service.AdChannelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/channel")
@Api(value = "频道管理", tags = "channel", description = "频道管理API")
public class AdChannelController {

    @Autowired
    private AdChannelService adChannelService;

    /**
     * 分页查询频道列表
     * @param channelDto channelDto继承了PageRequestDto,其中方法实现了分页参数的赋值,所以,只需要前端传递的name值
     * @return
     */
    @PostMapping("/list")
    @ApiOperation("频道分页列表查询")
    public ResponseResult list(@RequestBody ChannelDto channelDto){
        return adChannelService.list(channelDto);
    }

    /**
     * 新增频道
     * @param adChannel
     * @return
     */
    @PostMapping("/save")
    @ApiOperation("新增频道")
    public ResponseResult insert(@RequestBody AdChannel adChannel){
        return adChannelService.insert(adChannel);
    }

    /**
     * 修改频道信息
     */
    @PostMapping("/update")
    @ApiOperation("修改频道信息")
    public ResponseResult update(@RequestBody AdChannel adChannel){
        return adChannelService.updata(adChannel);
    }

    /**
     * 根据id删除频道
     */
    @GetMapping("/del/{id}")
    @ApiOperation("根据id删除频道")
    public ResponseResult del(@PathVariable Integer id){
        return adChannelService.del(id);
    }
}
