package com.heima.admin.controller.v1;

import com.heima.admin.service.AdSensitiveService;
import com.heima.model.admin.dtos.SensitiveDto;
import com.heima.model.admin.pojo.AdSensitive;
import com.heima.model.common.dtos.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/sensitive")
@Api(value = "敏感词管理", tags = "sensitive", description = "敏感词管理API")
public class AdSensitiveController {

    @Autowired
    private AdSensitiveService adSensitiveService;

    /**
     * 分页查询敏感词列表
     */
    @PostMapping("/list")
    @ApiOperation("敏感词管理分页列表查询")
    public ResponseResult list(@RequestBody SensitiveDto sensitiveDto){
        return adSensitiveService.pageList(sensitiveDto);
    }

    @PostMapping("/save")
    @ApiOperation("新增敏感词")
    public ResponseResult insert(@RequestBody AdSensitive adSensitive){
        return adSensitiveService.insert(adSensitive);
    }

    @PostMapping("/update")
    @ApiOperation("修改敏感词")
    public ResponseResult update(@RequestBody AdSensitive adSensitive){
        return adSensitiveService.updateSen(adSensitive);
    }

    @DeleteMapping("/del/{id}")
    @ApiOperation("删除敏感词")
    public ResponseResult del(@PathVariable Integer id){
        return adSensitiveService.del(id);
    }
}
