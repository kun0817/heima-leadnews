package com.heima.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.admin.pojo.AdChannel;
import com.heima.model.admin.dtos.ChannelDto;
import com.heima.model.common.dtos.ResponseResult;

public interface AdChannelService extends IService<AdChannel> {

    /**
     * 分页查询频道列表
     * @param channelDto
     * @return
     */
    ResponseResult list(ChannelDto channelDto);

    /**
     * 新增频道
     * @return
     * @param adChannel
     */
    ResponseResult insert(AdChannel adChannel);

    /**
     * 修改频道信息
     * @param adChannel
     * @return
     */
    ResponseResult updata(AdChannel adChannel);

    /**
     * 根据id删除指定频道
     * @param id
     * @return
     */
    ResponseResult del(Integer id);
}
