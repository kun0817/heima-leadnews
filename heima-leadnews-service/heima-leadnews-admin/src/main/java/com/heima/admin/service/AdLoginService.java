package com.heima.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.admin.dtos.AdUserDto;
import com.heima.model.admin.pojo.AdUser;
import com.heima.model.common.dtos.ResponseResult;

public interface AdLoginService extends IService<AdUser> {

    /**
     * 用户登录
     * @param adUserDto
     * @return
     */
    ResponseResult login(AdUserDto adUserDto);

    /**
     * 用户注册
     * @param adUser
     * @return
     */
    ResponseResult register(AdUser adUser);
}
