package com.heima.common.exception;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 统一异常处理类
 */
@RestControllerAdvice
@Slf4j
public class ExceptionCatch {

    /**
     * 捕获不可预见异常
     * @param e
     * @return
     */
    @ExceptionHandler(value = Exception.class)
    public ResponseResult exception(Exception e){
        //控制台输出错误信息
        e.printStackTrace();
        //日志输出错误信息
        log.info(e.getMessage());
        //统一返回前端结果
        return ResponseResult.errorResult(AppHttpCodeEnum.SERVER_ERROR,"数据库正忙,请稍后再试");
    }

    /**
     * 捕获可预见异常
     * @param ex
     * @return
     */
    @ExceptionHandler(CustomException.class)
    public ResponseResult customException(CustomException ex){

        System.out.println(ex);
        //记录日志
        log.error("catch exception:{}",ex);
        //返回通用异常
        return ResponseResult.errorResult(ex.getAppHttpCodeEnum());

    }
}
